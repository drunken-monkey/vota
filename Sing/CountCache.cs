//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using Vota.Common;

namespace Sing 
{
    ///<summary>
    /// Class which can cache STV counting results to avoid redudantant recalculations
    ///</summary>
    public class CountCache
    {
        private Dictionary<KeyValuePair<Type, uint>, IList<Candidate>> cache;

        public CountCache()
        {
            cache = new Dictionary<KeyValuePair<Type, uint>, IList<Candidate>>();
        }

        ///<summary>
        /// Try to get a caches result for a specific counter
        ///</summary>
        ///<returns> The cached result or null if none is available</returns>
        public IList<Candidate> GetResult(StvCounter counter) 
        {
            return GetResult(counter, counter.Seats);
        }

        ///<summary>
        /// Try to get a caches result for a specific counter and a specified number of seats
        ///</summary>
        ///<returns> The cached result or null if none is available</returns>
        public IList<Candidate> GetResult(StvCounter counter, uint seats) 
        {
            return GetResult(counter.GetType(), seats);
        }

        ///<summary>
        /// Try to get a caches result for a specific counter and a specified number of seats
        ///</summary>
        ///<returns> The cached result or null if none is available</returns>
        public IList<Candidate> GetResult(Type counterType, uint seats) 
        {
            var dictKey = new KeyValuePair<Type, uint>(counterType, seats);
            IList<Candidate> result = null;
            if(cache.TryGetValue(dictKey, out result))
                return result;
            else
                return null;
        }
        
        ///<summary>
        /// Cache a specified result.
        /// Previous results for the same counter will be overwritte (if present).
        ///</summary>
        public void AddResult(StvCounter counter, IList<Candidate> result) 
        {
            AddResult(counter, result, counter.Seats);
        }

        ///<summary>
        /// Cache a specified result.
        /// Previous results for the same counter will be overwritte (if present).
        ///</summary>
        public void AddResult(StvCounter counter, IList<Candidate> result, uint seats) 
        {
            AddResult(counter.GetType(), result, seats);
        }

        ///<summary>
        /// Cache a specified result.
        /// Previous results for the same counter will be overwritte (if present).
        ///</summary>
        public void AddResult(Type counterType, IList<Candidate> result, uint seats) 
        {
            if(result == null)
                throw new ArgumentNullException("Result to cache must not be null", nameof(result));
            var dictKey = new KeyValuePair<Type, uint>(counterType, seats);
            cache[dictKey] = result;
        }
    }
}