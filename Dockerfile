FROM microsoft/dotnet:2.2-sdk AS build-env

ENV DOTNET_CLI_TELEMETRY_OPTOUT true

COPY *.sln /app/
COPY NuGet.Config /app/
COPY Sing/*.csproj /app/Sing/
COPY Vota.Common/*.csproj /app/Vota.Common/
COPY Webservice/Vota/*.csproj /app/Webservice/Vota/
COPY Webservice/Vota.ServiceInterface/*.csproj /app/Webservice/Vota.ServiceInterface/
COPY Webservice/Vota.ServiceModel/*.csproj /app/Webservice/Vota.ServiceModel/

RUN useradd -ms /bin/bash builduser
RUN chown -R builduser /app
USER builduser

WORKDIR /app/Webservice/Vota
RUN dotnet restore

COPY --chown=builduser . /app

WORKDIR /app/Webservice/Vota
RUN dotnet publish -c Release  -p:Version=`git describe` -o out


# Build runtime image
FROM microsoft/dotnet:2.2-aspnetcore-runtime

ENV DOTNET_CLI_TELEMETRY_OPTOUT true

RUN useradd -ms /bin/bash runuser

WORKDIR /app
COPY --from=build-env --chown=runuser /app/Webservice/Vota/out .

USER runuser

ENV ASPNETCORE_URLS http://*:5000
ENTRYPOINT ["dotnet", "Vota.dll"]