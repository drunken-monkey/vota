# Vota

A software to calculate the results of elections. It is meant to be used for the Green Party of Vienna.

Its main components are:

* Vota.CLI: A commandline tool
* Vota: A RESTful webservice
* Sing: An implementation of the *Single Transferable Vote* (STV) system (including a ranked and quoted variant)

## Requirements

You need .NET Core 2.2 to build (SDK) and run (Runtime) this software. For the webservice, ASP.NET Core 2.2 must be installed, too.  See [here](https://dotnet.microsoft.com/download/linux-package-manager/ubuntu18-04/sdk-current) for information.

While it should in theory work on a windows machine, the software was only tested on Ubuntu Bionic 18.04.

If you want to use the dockerized version of Vota, you obviously have to install docker.

## Build

    dotnet publish -c Release -p:Version=`git describe`

### Docker

There is also a dockerized build available for the webservice:

    docker build -t vota_webservice .
    docker build -f Vota.CLI/Dockerfile -t vota_cli .

## Run

### Docker

The dockerized version of the webservice can be started like this:

    docker run -it --publish 127.0.0.1:5000:5000 --detach --name vota_webservice vota

The dockerized version of the CLI client can be started like this:

    docker run -it --volume /tmp/count:/data vota_cli --seats 8 --electiontype RankedSTV --candidates /data/candidates.csv -x /data/votes.csv --config /data/config.txt --protocol /data/protocol.txt

The directory `/tmp/count` contains the input and output files for the client. It can be replaced by any local directory. For details on the parameters, check the [README of the client](Vota.CLI/README.md).