//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using NDesk.Options;
using ServiceStack.Text;
using Sing;
using Vota.Common;
using CsvReader = Vota.Common.CsvReader;

namespace Vota.CLI
{
    class VotaCli
    {
        static void Main(string[] args)
        { 
            string listfileName = null;
            string spreadsheetfileName = null;
            string candidatesFilename = null;
            string protocolFilename = null;
            string configFilename = null;
            ElectionType type = ElectionType.RankedSTV;
            bool calculatePath = false;
            uint seats = 8;
            bool help   = false; 
            var p = new OptionSet () {
   	            { "f|listfile=", "A CSV file with the list of votes", v => listfileName = v },
                { "x|spreadsheetfile=", "A CSV file with the votes based on a spreadsheet", v => spreadsheetfileName = v },
                { "c|candidates=", "A CSV file with the list of candidates", v => candidatesFilename = v },
                { "s|seats=", "The number of seats to be calculated", v => seats = UInt32.Parse(v) },
                { "p|protocol=", "File where the protocol should be stored. Will be printed to stdout if not specified", 
                    v => protocolFilename = v},
                { "C|config=", "File where the configuration should be stored. Goes to stdout if not specified", 
                    v => configFilename = v},
                { "t|electiontype=", "Specify the type of election: STV, QuotedSTV, RankedSTV (default)", 
                    v => ElectionType.TryParse(v, true, out type)},
                { "P|path", "Wether to also print the path to the result (all results for seats 1-n).",
                    v => calculatePath = true},
                { "h|?|help", "Print help", v => help = true },
            };
            p.Parse (args);

            if (help) {
                Console.WriteLine("HELP:");
                p.WriteOptionDescriptions(Console.Out);
                System.Environment.Exit(0);
            }

            IElection election = new OfflineElection() { Type = type, Seats = seats };

            if (spreadsheetfileName != null && !File.Exists(spreadsheetfileName)) {
                Console.Error.WriteLine("ERROR: File '{0}' does not exist.", spreadsheetfileName);
                Environment.Exit(1);
            }
            
            if(listfileName != null && !File.Exists(listfileName)) {
                Console.Error.WriteLine("ERROR: File '{0}' does not exist.", listfileName);
                Environment.Exit(1);
            }
            
            IList<Candidate> candidates = null;
            if (candidatesFilename != null) {
                if (File.Exists(candidatesFilename)) {
                    string candidatesCsv = File.ReadAllText(candidatesFilename);
                    candidates = CsvReader.ParseCandidates(candidatesCsv);
                } else {
                    Console.Error.WriteLine("ERROR: File with candidates '{0}' does not exist.", candidatesFilename);
                    Environment.Exit(1);
                }
            } else {
                Console.Error.WriteLine("ERROR: Please specify a file with candidates.");
                p.WriteOptionDescriptions(Console.Out);
                Environment.Exit(1);
            }

            List<VotingSlip> votingslips = new List<VotingSlip>();

            if (listfileName != null) {
                string csv = File.ReadAllText(listfileName);
                votingslips.AddRange(CsvReader.ParseCsvList(csv));
            }
            if (spreadsheetfileName != null) {
                string csv = File.ReadAllText(spreadsheetfileName);
                votingslips.AddRange(CsvReader.ParseCsvSpreadsheet(csv));
            }

            if(!votingslips.Any()) {
                Console.Error.WriteLine("ERROR: No votes entered.");
                p.WriteOptionDescriptions(Console.Out);
                Environment.Exit(1);
            }

            election.Votes = votingslips;            
            
            if (candidates == null)
                candidates = votingslips.GetCandidates();

            election.Candidates = candidates.ToHashSet();

            if (configFilename != null)
                File.WriteAllText(configFilename, JsonSerializer.SerializeToString(election).IndentJson());
            else
                Console.WriteLine("### Config:\n{0}", JsonSerializer.SerializeToString(election).IndentJson());
            
            Console.WriteLine("### The following candidates are running:\n");
            for(int i=0; i< candidates.Count; ++i)
                Console.WriteLine("{0}.\t{1}{2}", i+1, candidates[i], (candidates[i].EligibleForQuote?(" (*)"):""));

            uint minSeatsForCalculation = calculatePath ? 1 : seats;
            CalculationResult result = null;
            for(uint seatCount=minSeatsForCalculation; seatCount <= seats; ++seatCount) {
                election.Seats = seatCount;
                result = election.CalculateSeats();
                Console.WriteLine("### Result of election ({0} seats) \n", seatCount);
                for(int i=0; i< result.Elected.Count; ++i)
                    Console.WriteLine("{0}.\t{1}", i+1, result.Elected[i]);
            }
            
            if (protocolFilename != null)
                File.WriteAllText(protocolFilename, result.Protocol.ToJson());
            else
                Console.WriteLine("=======\n\nProtocol of count:\n{0}", result.Protocol.ToJson());
        }
    }
}