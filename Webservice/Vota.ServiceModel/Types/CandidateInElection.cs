//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Runtime.Serialization;
using ServiceStack.DataAnnotations;

namespace Vota.ServiceModel.Types
{
    [DataContract]
    public class CandidateInElection
    {
        [DataMember]
        [AutoId]
        public Guid Id { get; set; }
        
        [DataMember]
        [Required]
        [References(typeof(Election))]
        public Guid ElectionId { get; set; }
        
        [DataMember]
        
        [Required]
        [References(typeof(Candidate))]
        public Guid CandidateId { get; set; }
    }
}