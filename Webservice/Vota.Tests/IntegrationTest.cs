//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using Funq;
using ServiceStack;
using NUnit.Framework;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using Vota.ServiceInterface;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;

namespace Vota.Tests
{
    public class IntegrationTest
    {
        const string BaseUri = "http://localhost:2000/";
        private readonly ServiceStackHost appHost;

        class AppHost : AppSelfHostBase
        {
            public AppHost() : base(nameof(IntegrationTest), typeof(ElectionsService).Assembly) { }

            public override void Configure(Container container)
            {
                container.Register<IDbConnectionFactory>(
                    //new OrmLiteConnectionFactory(MapProjectPath("~/App_Data/Northwind.sqlite"), SqliteDialect.Provider));
                    new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));
            
                // Populate the database
                using (var db = Resolve<IDbConnectionFactory>().OpenDbConnection()) {
                    // Create tables if they do not exist
                    db.CreateTableIfNotExists(typeof(Candidate));
                    db.CreateTableIfNotExists(typeof(Election));
                
                    // Add Test data into the db
                    db.Insert(new Election() {Name = "Party board"});
                    db.Insert(new Election() {Name = "Parliamentary candidates"});
                }
            }
        }

        public IntegrationTest()
        {
            appHost = new AppHost()
                .Init()
                .Start(BaseUri);
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        public IServiceClient CreateClient() => new JsonServiceClient(BaseUri);

        [Test]
        public void Can_call_Hello_Service()
        {
            var client = CreateClient();

            var elections = client.Get(new ElectionsRequest());
            Assert.That(elections.IDs.Length, Is.EqualTo(2));
        }
    }
}