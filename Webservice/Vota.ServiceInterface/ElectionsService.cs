//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Linq;
using ServiceStack;
using ServiceStack.OrmLite;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;

namespace Vota.ServiceInterface
{
    public class ElectionsService : Service
    {
        public object Get(ElectionsRequest request)
        {
            var elections = Db.Select<Election>()
                .Select(e => e.Id)
                .ToArray();
            return new ElectionsResponse() {IDs = elections};
        }
    }
}
