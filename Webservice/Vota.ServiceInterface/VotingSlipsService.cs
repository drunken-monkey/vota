//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Linq;
using ServiceStack;
using ServiceStack.OrmLite;
using ServiceStack.Text;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;

namespace Vota.ServiceInterface
{
    public class VotingSlipsService : Service
    {
        public VotingSlipsResponse Get(VotingSlipsGetRequest request)
        {
            var slips = Db.Select<VotingSlip>(s => s.ElectionId == request.Id).ToList();
            return new VotingSlipsResponse() {Slips = slips};
        }

        public void Post(VotingSlipsPostRequest request)
        {
            //Check if all candidates are running
            var candidatesInElection = ResolveService<ElectionsCandidatesService>()
                .Get(new ElectionsCandidatesRequest() {Id = request.Id})
                .Candidates.Select(c => c.Name);
            if (request.Slip.Votes.Except(candidatesInElection).Any()) {
                request.Slip.Votes.Except(candidatesInElection).PrintDump();
                candidatesInElection.PrintDump();
                throw new ArgumentException("Not all candidates are running in this election");
            }
            
            //Check if the voting slip name is new (and non-empty)
            if(request.Slip.Name.IsNullOrEmpty())
                throw new ArgumentException("Slip name not approriate.");
            if(Db.Count<VotingSlip>(s => s.Name == request.Slip.Name) > 0)
                throw new ArgumentException("Slip name already exists.");

            request.Slip.ElectionId = request.Id;
            Db.Insert(request.Slip);
        }
    }
}
