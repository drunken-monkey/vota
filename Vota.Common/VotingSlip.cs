//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Vota.Common
{
    public class VotingSlip
    {
        /// <summary>
        /// Name of the voting slip (e.g. A-003)
        /// </summary>
        public string Name { get; set; }
        
        private List<string> votes = new List<string>();

        public double Weight { get; private set; }

        public VotingSlip(IEnumerable<Candidate> votes, double weight = 1) : this(votes.Select(v => v.Name), weight)
        {
            
        }
        
        public VotingSlip(IEnumerable<string> votes, double weight = 1)
        {
            if (votes == null)
                throw new ArgumentNullException("votes");
            if (votes.Distinct().Count() != votes.Count())
                throw new ArgumentException("Candidates must not appear mulitple times on the voting slip.", "votes");
            if(weight < 0 || weight > 1)
                throw new ArgumentException("Weight must be between 0 and 1.", "weight");

            this.votes.AddRange(votes);
            this.Weight = weight;
        }

        public VotingSlip() : this(new string[0])
        {
        }

        public VotingSlip(params string[] votes) : this((IEnumerable<string>)(votes))
        {
        }

        public VotingSlip(params Candidate[] votes) : this((IEnumerable<Candidate>) votes)
        {
            
        }

        public VotingSlip(double weight, params string[] votes) : this((IEnumerable<string>)(votes))
        {
            this.Weight = weight;
        }
        
        public VotingSlip(double weight, params Candidate[] votes) : this((IEnumerable<Candidate>)(votes))
        {
            this.Weight = weight;
        }

        public IList<string> Votes
        {
            get
            {
                return new List<string>(votes);
            }
        }
        
        public VotingSlip RemoveCandidates(IEnumerable<Candidate> candidates)
        {
            if (candidates == null)
                throw new ArgumentNullException("candidates");
            return new VotingSlip(votes.Except(candidates.Select(c => c.Name)), Weight);
        }

        public VotingSlip TransferVote(Candidate candidate, double percentage = 1)
        {
            if(candidate == null)
                throw new ArgumentNullException("candidate");
            return TransferVote(candidate.Name, percentage);
        }
            
        /// <summary>
        /// Transfers votes away from a candidate to be removed (either because he is already elected or removed)
        /// If the candidate was the top candidate, he is removed and the weight of the slip is reduced by percentage.
        /// If the candidate was not the top candidate, he is simply removed and the weight remains unaltered.
        /// If the candidate was never part of this slip, the candidates remain the same.!--
        /// In any case, a copy of the slip with the correct candidates is returned.
        /// </summary>
        /// <param name="candidate">The candidate whose votes are to be transferred to the next candidate.</param>
        /// <param name="percentage">Must be larger between 0 and 1.!-- Defaults to 1.</param>
        /// <returns></returns>
        public VotingSlip TransferVote(string candidate, double percentage = 1)
        {
            if(candidate == null)
                throw new ArgumentNullException("candidate");
            if(percentage < 0 || percentage > 1)
                throw new ArgumentException("Percentage must be between 0 and 1.", nameof(percentage));

            if(!votes.Any())
                return this;
            
            double newWeight = this.Weight;
            IList<string> newVotes = Votes;
            if(votes[0].Equals(candidate))
                newWeight = this.Weight * percentage;
            
            newVotes.Remove(candidate);
            return new VotingSlip(newVotes, newWeight);
        }

        public VotingSlip Remove(Candidate candidate)
        {
            if(candidate == null)
                throw new ArgumentNullException(nameof(candidate));
            return Remove(candidate.Name);
        }
        
        public VotingSlip Remove(string candidate)
        {
            if(candidate == null)
                throw new ArgumentNullException(nameof(candidate));
        
            if(!votes.Any())
                return this;
            
            IList<string> newVotes = Votes;
            newVotes.Remove(candidate);
            return new VotingSlip(newVotes, Weight);
        }

        ///<summary>Removes all votes from a voting slip for candidates which are not electable</summary>
        public VotingSlip RestrictCandidates(IEnumerable<Candidate> electableCandidates)
        {
            if(electableCandidates == null)
                throw new ArgumentNullException(nameof(electableCandidates));
            var electableNames = electableCandidates.Select(c => c.Name);
            
            var newVotes = new List<string>();
            foreach(string vote in Votes)
                if(electableNames.Contains(vote))
                    newVotes.Add(vote);
            
            return new VotingSlip(newVotes, this.Weight);
        }

        public override bool Equals(object obj)
        {
            VotingSlip other = obj as VotingSlip;
            if(other == null)
                return false;
            return (this.Weight == other.Weight) && this.votes.SequenceEqual(other.votes);
        }

        public override int GetHashCode()
        {
            int ret = 0;
            foreach(string vote in votes)
                ret ^= vote.GetHashCode();
            
            ret ^= Weight.GetHashCode();
            
            return ret;
        }

        public override string ToString()
        {
            StringBuilder ret = new StringBuilder();
            for(int i=0; i<votes.Count; ++i)
                ret.AppendFormat("{0}: '{1}'  ", i+1, votes[i].ToString());
            return ret.ToString().Trim();
        }
    }
}

