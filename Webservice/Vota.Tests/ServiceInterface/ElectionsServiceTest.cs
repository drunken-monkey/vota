//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Testing;
using Vota.ServiceInterface;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;

namespace Vota.Tests.ServiceInterface
{
    public class ElectionsServiceTest
    {
        private ServiceStackHost appHost;

        [OneTimeSetUp]
        public void Setup()
        {
            appHost = new BasicAppHost().Init();
            appHost.Container.AddTransient<ElectionsService>();

            appHost.Container.Register<IDbConnectionFactory>(
                //new OrmLiteConnectionFactory(MapProjectPath("~/App_Data/Northwind.sqlite"), SqliteDialect.Provider));
                new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));
            
            // Populate the database
            using (var db = appHost.Container.Resolve<IDbConnectionFactory>().OpenDbConnection()) {
                // Create tables if they do not exist
                db.CreateTableIfNotExists(typeof(Candidate));
                db.CreateTableIfNotExists(typeof(Election));
                
                // Add Test data into the db
                db.Insert(new Election() {Name = "Party board"});
                db.Insert(new Election() {Name = "Parliamentary candidates"});
            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        [Test]
        public void GetTest()
        {
            var service = appHost.Container.Resolve<ElectionsService>();

            var response = (ElectionsResponse)service.Get(new ElectionsRequest());

            Assert.That(response.IDs.Length, Is.EqualTo(2));
        }
    }
}
