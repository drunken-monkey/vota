//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections.Generic;
using Vota.Common;

namespace Sing
{
    public static class StvCounterFactory
    {
        public static StvCounter CreateCounter(ElectionType type, uint seats, IEnumerable<VotingSlip> slips)
        {
            StvCounter counter;
            switch (type) {
                case ElectionType.STV:
                    counter = new StvCounter(seats, slips);
                    break;
                case ElectionType.QuotedSTV:
                    counter = new QuotedStvCounter(seats, slips);
                    break;
                case ElectionType.RankedSTV:
                    counter = new RankedStvCounter(seats, slips);
                    break;
                default:
                    throw new ApplicationException("Unknown election type.");
            }

            return counter;
        }
    }
}