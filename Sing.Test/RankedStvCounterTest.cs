//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System.Collections.Generic;
using Xunit;
using Vota.Common;


namespace Sing.Test
{
    public class RankedStvCounterTest
    {
        [Theory]
        [InlineData(true)]
        [InlineData(false)]  
        public void CalculateRankingTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Homer", false),
                new Candidate("Marge", true),
                new Candidate("Bart", false),
                new Candidate("Lisa", true),
                new Candidate("Maggie", true),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
            for(int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Bart", "Homer", "Lisa"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Marge", "Bart", "Lisa", "Homer"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Homer", "Marge", "Lisa"));
            
            // Manual Counting:
            // 1: B
            // 2: H, B
            // 3: H, M, B
            // 4: H, M, B, L
            // -> Ranked: 1. B, 2. M, 3. L, 4.H 

            RankedStvCounter counter = new RankedStvCounter(1, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);

            counter = new RankedStvCounter(2, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            
            counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(3, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            Assert.Equal(new Candidate("Lisa"), elected[2]);
            
            counter = new RankedStvCounter(4, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(4, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            Assert.Equal(new Candidate("Lisa"), elected[2]);
            Assert.Equal(new Candidate("Homer"), elected[3]);
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CalculateRankingNotAllSeatsTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Homer", false),
                new Candidate("Marge", true),
                new Candidate("Bart", false),
                new Candidate("Lisa", true),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Homer", "Marge"));
            for(int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Lisa", "Bart", "Marge"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Bart", "Homer"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Marge", "Bart", "Homer"));
            for (int i = 0; i < 6; ++i)
                votes.Add(new VotingSlip("Homer", "Marge"));
            
            // Manual Counting:
            // 1: B
            // 2: H, B
            // 3: H, M, B
            // 4: H, M, B
            // -> Ranked: 1. B, 2. M 

            RankedStvCounter counter = new RankedStvCounter(1, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);

            counter = new RankedStvCounter(2, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            
            counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
            
            counter = new RankedStvCounter(4, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Bart"), elected[0]);
            Assert.Equal(new Candidate("Marge"), elected[1]);
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CalculateRankingAllSameWithQuoteReorderingTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Homer", false),
                new Candidate("Marge", true),
                new Candidate("Bart", false),
                new Candidate("Lisa", true),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 100; ++i)
                votes.Add(new VotingSlip("Homer", "Bart", "Lisa", "Marge"));

            RankedStvCounter counter = new RankedStvCounter(1, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);

            counter = new RankedStvCounter(2, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(2, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);
            Assert.Equal(new Candidate("Lisa"), elected[1]);
            
            counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(3, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);
            Assert.Equal(new Candidate("Lisa"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
            
            counter = new RankedStvCounter(4, votes, useCache);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(4, elected.Count);
            Assert.Equal(new Candidate("Homer"), elected[0]);
            Assert.Equal(new Candidate("Lisa"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
            Assert.Equal(new Candidate("Bart"), elected[3]);
        }
        
        [Theory]
        [InlineData(true)]
        [InlineData(false)]
        public void CalculateRankingTieBreakerFirstLevelVotesTest(bool useCache)
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Marge", true),
                new Candidate("Lisa", true),
                new Candidate("Maggie", true),
                new Candidate("Bart", false),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 8; ++i)
                votes.Add(new VotingSlip("Lisa", "Marge"));
            for (int i = 0; i < 10; ++i)
                votes.Add(new VotingSlip("Lisa", "Maggie"));
            for(int i = 0; i < 8; ++i)
                votes.Add(new VotingSlip("Bart"));
            for (int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Marge", "Bart"));
            for (int i = 0; i < 4; ++i)
                votes.Add(new VotingSlip("Maggie", "Bart"));

            // Manual Counting:
            // Lisa has 18 Votes -> elected first
            // Quota for 3 seats is 9 -> 4 votes transferred to Marge, 5 to Maggie
            // -> Marge wins second seat because of more first level votes
            // Simple Counts:
            // 1: L
            // 2: L, B
            // 3: L, Mar, Mag
            // Ranking: L, B, Mar

            RankedStvCounter counter = new RankedStvCounter(3, votes, useCache);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(3, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);
            Assert.Equal(new Candidate("Bart"), elected[1]);
            Assert.Equal(new Candidate("Marge"), elected[2]);
        }
        

        [Fact]
       public void CalculateSeatsWhereOnlyOneElectedForTwoSeatsTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 17; ++i)
               votes.Add(new VotingSlip("Homer"));
           for(int i = 0; i < 12; ++i)
               votes.Add(new VotingSlip("Lisa", "Homer"));
           for (int i = 0; i < 11; ++i)
               votes.Add(new VotingSlip("Bart", "Homer"));
            
           // Manual Counting:
           // 1: H
           // 2: H
           // 3: H, L, B
                      
           StvCounter counter = new RankedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Equal(1, elected.Count);
           Assert.Equal(new Candidate("Homer", false), elected[0]);

           counter = new RankedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count);
           Assert.Equal(new Candidate("Homer", false), elected[0]);
           Assert.Equal(new Candidate("Lisa", false), elected[1]);

           counter = new RankedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(2, elected.Count);
           Assert.Equal(new Candidate("Homer", false), elected[0]);
           Assert.Equal(new Candidate("Lisa", false), elected[1]);
       }

       [Fact]
       public void CalculateSeatsWithNoneElectedForFirstSeatTest()
       {
            Candidate[] candidates = new Candidate[] {
               new Candidate("Homer", false),
               new Candidate("Marge", true),
               new Candidate("Bart", false),
               new Candidate("Lisa", true),
               new Candidate("Maggie", true),
           };
           List<VotingSlip> votes = new List<VotingSlip>();
           for (int i = 0; i < 12; ++i)
               votes.Add(new VotingSlip("Homer"));
           for(int i = 0; i < 9; ++i)
               votes.Add(new VotingSlip("Lisa"));
           for (int i = 0; i < 9; ++i)
               votes.Add(new VotingSlip("Bart"));
            
           // Manual Counting:
           // 1: 
           // 2: H
           // 3: H, L, B
           // -> No one elected for ranked order
                      
           StvCounter counter = new RankedStvCounter(1, votes);
           counter.Candidates = candidates;
           var elected = counter.CalculateSeats().Elected;
           Assert.Equal(0, elected.Count);

           counter = new RankedStvCounter(2, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(0, elected.Count);

           counter = new RankedStvCounter(3, votes);
           counter.Candidates = candidates;
           elected = counter.CalculateSeats().Elected;
           Assert.Equal(0, elected.Count);
       }

       [Fact]
        public void CalculateRankingWithNoneElectedForSecondSeatTest()
        {
            Candidate[] candidates = new Candidate[] {
                new Candidate("Marge", true),
                new Candidate("Lisa", true),
                new Candidate("Maggie", true),
                new Candidate("Bart", false),
            };
            
            List<VotingSlip> votes = new List<VotingSlip>();
            for (int i = 0; i < 10; ++i)
                votes.Add(new VotingSlip("Lisa", "Marge"));
            for(int i = 0; i < 5; ++i)
                votes.Add(new VotingSlip("Maggie"));
            for (int i = 0; i < 3; ++i)
                votes.Add(new VotingSlip("Bart"));

            // Manual Counting:
            // 1: L
            // 2: L
            // 3: L, Mar, Mag

            RankedStvCounter counter = new RankedStvCounter(1, votes);
            counter.Candidates = candidates;
            var elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);

            counter = new RankedStvCounter(2, votes);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);

            counter = new RankedStvCounter(3, votes);
            counter.Candidates = candidates;
            elected = counter.CalculateSeats().Elected;
            Assert.Equal(1, elected.Count);
            Assert.Equal(new Candidate("Lisa"), elected[0]);
        }
    }
}