# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

## [1.1.0] - 2019-06-20

### Added

- CLI uses a cache to prevent unneccesary repetition of counts
- Add dockerized version of Vota.CLI
- Improved error handling of Vota.CLI (exits if essential info is mising)

### Changed

- Bugfixes for corner cases in counting
  - Bugfix for quoted counter (if initial count returned too few elected)
  - Terminate ranked counting if no one is elected in a count
  - Fix quoted counting if to few people have been elected in first count
  - Fix quoted counting if quote fulfillments requires multiple additional counts
  - Fix tiebreaker handling for ranked STV

### Removed

## [1.0.0] - 2019-06-18

### Added

- Initial release

[Unreleased]: https://gitlab.com/mtausig/vota/compare/1.1.0...develop
[1.1.0]: https://gitlab.com/mtausig/vota/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/mtausig/vota/tree/1.0.0
