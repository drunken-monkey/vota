//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using Xunit;

namespace Vota.Common.Test
{
    public class CandidateTest
    {
        [Fact]
        public void GetNameTest()
        {
            Candidate candidate = new Candidate("foo");
            Assert.Equal("foo", candidate.Name);
        }

        [Fact]
        public void ConstructorChecksNullTest() 
        {
            Assert.Throws<ArgumentNullException>(() => new Candidate(null));
        }

        [Fact]
        public void ConstructorChecksEmptyNameTest() 
        {
            Assert.Throws<ArgumentException>(() => new Candidate(""));
        }

        [Fact]
        public void EqualsTest()
        {
            Candidate c1 = new Candidate("foo");
            Candidate c2 = new Candidate("bar");
            Assert.NotEqual(c1, c2);

            c2  = new Candidate("foo");
            Assert.Equal(c1, c2);

        }
        
        [Fact]
        public void EqualsWithQuoteTest()
        {
            Candidate withQuote = new Candidate("foo", true);
            Candidate withoutQuote = new Candidate("foo", false);
            Candidate withoutQuoteImplicit = new Candidate("foo");
            Candidate withQuoteOtherName = new Candidate("bar", true);

            Assert.True(withQuote.Equals(withoutQuote));
            Assert.True(withQuote.Equals(withoutQuoteImplicit));
            Assert.False(withQuote.Equals(withQuoteOtherName));
        }
    }
}
