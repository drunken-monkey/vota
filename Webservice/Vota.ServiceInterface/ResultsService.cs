//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Linq;
using ServiceStack;
using Sing;
using Vota.Common;
using Vota.ServiceModel;

namespace Vota.ServiceInterface
{
    public class ResultService : Service
    {
        public ResultResponse Get(ResultRequest request)
        {
            var election = ResolveService<ElectionsDetailsService>().Get(new ElectionsDetailsRequest() {Id = request.Id});
            var candidates = ResolveService<ElectionsCandidatesService>().Get(new ElectionsCandidatesRequest() {Id = request.Id})
                .Candidates.Select(c => c.ToCandidate()).ToArray();
            var slips = ResolveService<VotingSlipsService>().Get(new VotingSlipsGetRequest() {Id = request.Id})
                .Slips.Select(s => s.ToVotingSlip()).ToArray();

            StvCounter counter = StvCounterFactory.CreateCounter(election.Type, (uint)election.MaxCandidatesElected, slips);
            
            counter.Candidates = candidates;
            var calculationResult = counter.CalculateSeats();
            return new ResultResponse() {
                Ordered = counter.Ordered,
                Elected = calculationResult.Elected.Select(c => c.Name).ToList(),
                Protocol = calculationResult.Protocol.ToJson(),
            };
        }
    }
}
