//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using NUnit.Framework;
using ServiceStack;
using ServiceStack.Data;
using ServiceStack.OrmLite;
using ServiceStack.Testing;
using Sing;
using Vota.Common;
using Vota.ServiceInterface;
using Vota.ServiceModel;
using Vota.ServiceModel.Types;

namespace Vota.Tests.ServiceInterface
{
    public class ElectionsDetailsServiceTest
    {
        private ServiceStackHost appHost;
        
        private Election[] testData = {
            new Election() {
                Name = "Party board", Type = ElectionType.QuotedSTV,
                MinCandidatesElected = 0, MaxCandidatesElected = 6
            },
            new Election() {
                Name = "Parliamentary candidates", Type = ElectionType.RankedSTV,
                MinCandidatesElected = 4, MaxCandidatesElected = 10
            },
        };
     
        [OneTimeSetUp]
        public void Setup()
        {
            appHost = new BasicAppHost().Init();
            var container = appHost.Container;
            container.AddTransient<ElectionsDetailsService>();

            container.Register<IDbConnectionFactory>(
                new OrmLiteConnectionFactory(":memory:", SqliteDialect.Provider));
            
            // Populate the database
            using (var db = container.Resolve<IDbConnectionFactory>().OpenDbConnection()) {
                // Create tables if they do not exist
                db.CreateTableIfNotExists(typeof(Election));
                
                // Add Test data into the db
                foreach(var row in testData)
                    db.Insert(row);

            }
        }

        [OneTimeTearDown]
        public void OneTimeTearDown() => appHost.Dispose();

        [Test]
        public void GetTest()
        {
            var service = appHost.Container.Resolve<ElectionsDetailsService>();

            foreach(var election in testData)
            {
                var response = (ElectionsDetailsResponse)service.Get(new ElectionsDetailsRequest() {Id = election.Id});
                Assert.That(response.Name, Is.EqualTo(election.Name));
                Assert.That(response.Type, Is.EqualTo(election.Type));
                Assert.That(response.MaxCandidatesElected, Is.EqualTo(election.MaxCandidatesElected));
                Assert.That(response.MinCandidatesElected, Is.EqualTo(election.MinCandidatesElected));
            }
        }
    }
}
