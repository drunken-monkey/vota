//This file is part of Vota.
//
// Copyright 2019 Mathias Tausig <mathias@tausig.at>
//
//Vota is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.
//
//Foobar is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.
//
//You should have received a copy of the GNU General Public License
//along with Vota.  If not, see <https://www.gnu.org/licenses/>.
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.IO.Pipes;
using System.Linq;

namespace Vota.Common
{
    public static class CsvReader
    {
        /// <summary>
        /// Parses a csv file for voting slips
        /// </summary>
        /// <param name="csv">
        /// The lines of the file are expected to be in the following format:
        /// candidate #1;candidate #2:candidate #3;...
        /// The lines can hold as many candidates as desired, each voting is terminated by a newline
        /// An empty line corresponds to an empty slip
        /// The values of the fields are the candidates names
        /// </param>
        /// <returns>The list of parsed voting slips</returns>
        public static IList<VotingSlip> ReadCsvFile(string filename, IEnumerable<Candidate> availableCandidates = null)
        {
            return ParseCsvList(File.ReadAllText(filename), availableCandidates);
        }

        /// <summary>
        /// Parses a csv formatted string for voting slips
        /// </summary>
        /// <param name="csv">
        /// The lines are expected to be in the following format:
        /// candidate #1;candidate #2:candidate #3;...
        /// The lines can hold as many candidates as desired, each voting slip is terminated by a newline
        /// An empty line corresponds to an empty slip
        /// The values of the fields are the candidates names
        /// </param>
        /// <returns>The list of parsed voting slips</returns>
        public static IList<VotingSlip> ParseCsvList(string csv, IEnumerable<Candidate> availableCandidates = null)
        {
            List<VotingSlip> ret = new List<VotingSlip>();
            foreach (string line in csv.Split('\n')) {
                if (line.Trim() != "") {
                    string[] fields = line.Trim().Split(';');
                    var candidates = new List<string>();
                    foreach (string field in fields) {
                        candidates.Add(field.Trim());
                    }

                    var slip = new VotingSlip(candidates);
                    if (availableCandidates != null)
                        slip = slip.RestrictCandidates(availableCandidates);
                    ret.Add(slip);
                } else
                    ret.Add(new VotingSlip());
            }

            return ret;
        }
        
        /// <summary>
        /// Parses a csv formatted string for voting slips created from a spreadsheet
        /// </summary>
        /// <param name="csv">
        /// The lines are expected to be in the following format:
        /// The first line holds the names of the candidates
        /// All other lines contain either an empty value of the rank of the candidate on that slip
        /// The first column may contain the name of the slip
        /// The lines are terminated by a newline
        /// An empty line corresponds to an empty slip
        /// 
        /// Example:
        /// 
        ///     ;candidate #1;candidate #2;candidate #3;candidate #4
        /// A-01; 1          ;     2      ; 4          ; 3
        ///     ; 2          ;     1      ;            ; 3
        /// A-02;            ;            ;            ;  
        /// A-04; 1          ;            ;            ; 2
        /// 
        /// The values of the fields are the candidates names
        /// </param>
        /// <returns>The list of parsed voting slips</returns>
        public static IList<VotingSlip> ParseCsvSpreadsheet(string csv,
            IEnumerable<Candidate> availableCandidates = null)
        {
            if(csv == null)
                throw new ArgumentNullException(nameof(csv));
            if(csv.Trim() == "")
                throw new ArgumentException("CSV must not be empty", nameof(csv));
            List<VotingSlip> ret = new List<VotingSlip>();
            
            var lines = csv.Split('\n');
            string nameLine = lines[0];
            var names = nameLine.Trim().Split(';')
                .Skip(1)   //First column must be empty for the slip name
                .Select(n => n.Trim())  // Remove whitespaces from names beginning/end
                .ToArray();   
            // Check if no name is found which is not running
            if (availableCandidates != null) {
                var badNames = names.Except(availableCandidates.Select(c => c.Name));
                if (badNames.Any())
                    throw new ArgumentException(
                        String.Format("Name(s) found ({0}) which do not correspond to a running candidate.",
                            String.Join(", ", badNames)),
                        nameof(csv));
            }

            // Parse all lines after the header line
            foreach (string line in lines.Skip(1)) {
                if (line.Trim() != "") {    // Ignore empty lines
                    var fields = line.Trim().Split(';')
                        .Select(f => f.Trim()).ToArray();
                    string slipName = fields[0];
                    fields = fields.Skip(1).ToArray();
                    if(fields.Length > names.Length)
                        throw new ArgumentException(String.Format("More ranks ({0}) then candidates available ({1}) in this line: '{2}'",
                            fields.Length, names.Length, line), nameof(csv));
                    
                    // Build a dictionary with the rank values and the names
                    Dictionary<string, int> namesWithValue = new Dictionary<string, int>();
                    try {
                        for (int i = 0; i < fields.Length; ++i) {
                            if (fields[i] != "") {
                                int rank = Int32.Parse(fields[i]);
                                namesWithValue.Add(names[i], rank);
                            }
                        }
                    }
                    catch (FormatException) {
                        // Value in some field was not numerical
                        throw new ArgumentException("Non numeric value found in ranking field.", nameof(csv));
                    }
                    //Check that the correct ranks are used (1, 2, 3, ..., n)
                    var ranks = namesWithValue.Values;
                    if (!ranks.Any()) {
                        //empty voting slip
                        ret.Add(new VotingSlip(){Name = slipName});
                        continue;
                    }
                        
                    if(ranks.Min() != 1 ||  // First rank must be 1 
                       ranks.Max() != ranks.Count() ||  //Last rank must be the number of ranked candidates
                       ranks.Distinct().Count() != ranks.Count()  // Make sure that every rank is used only once
                       )
                        throw new ArgumentException(String.Format("Illegal ranking in line '{0}'.", line),
                            nameof(csv));
                    //Order the candidates according to their rank
                    var rankedNames = namesWithValue.OrderBy(kvp => kvp.Value).Select(kvp => kvp.Key);
                    ret.Add(new VotingSlip(rankedNames) {Name = slipName});
                }
            }
            
            //Check for duplicate slip names (apart from empty names)
            var slipNames = ret.Select(s => s.Name).Where(n => n != null && n.Trim() != "");
            if(slipNames.Distinct().Count() != slipNames.Count())
                throw new ArgumentException(String.Format("Duplicate slip names ({0}) have been found",
                    String.Join(", ", slipNames.Except(slipNames.Distinct()).Distinct())), nameof(csv));

            return ret;
        }

        /// <summary>
        /// Parses a csv formatted string for candidates
        /// </summary>
        /// <param name="csv">
        /// The lines are expected to be in the following format:
        /// candidate #1;eligibility for quote
        /// The second field can either be "true" or "1" for an eligible candidate, empty otherwise
        /// </param>
        /// <returns>The list of parsed candidates</returns>
        public static IList<Candidate> ParseCandidates(string csv)
        {
            var ret = new List<Candidate>();
            foreach (string line in csv.Split('\n')) {
                if (line.Trim() != "") {
                    string[] fields = line.Trim().Split(';');
                    string name = fields[0].Trim();
                    if (ret.Any(c => c.Name == name))
                        //Duplicate name
                        continue;
                    bool eligibleForQuote = false;
                    if (fields.Length > 1)
                        if (fields[1].Trim().ToLower() == "true" || fields[1].Trim() == "1")
                            eligibleForQuote = true;
                    Candidate candidate = new Candidate(name, eligibleForQuote);
                    ret.Add(candidate);
                }

            }
            return ret;
        }

        ///<summary>Creates a candidate object with a specified name
        ///If availableCandidates is null, a new object will be created
        ///If availableCandidates is not null, an object from that collection with a matching name will be taken.
        ///</summary>
        ///<returns>A candidate object with the specified name, or null if none was available in availableCandidates</returns>
        private static Candidate GetCandidate(string name, IEnumerable<Candidate> availableCandidates)
        {
            if (availableCandidates == null)
                return new Candidate(name);
            else {
                var possibleCandidates = availableCandidates.Where(c => c.Name == name);
                if (possibleCandidates.Count() == 1)
                    return possibleCandidates.First();
                else
                    return null;
            }
        }
    }
}